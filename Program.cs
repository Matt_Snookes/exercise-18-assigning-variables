﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var num1 = 5;
            var num2 = 7;
            var num3 = 9;
            var num4 = 12;
            var num5 = 97;
            var num6 = 69;
            var a="i am thinking";

            Console.WriteLine($"The value of num1 = {num1}");
            Console.WriteLine($"The value of num2 = {num2}");
            Console.WriteLine($"The value of num3 = {num3}");
            Console.WriteLine($"The value of num4 = {num4}");
            Console.WriteLine($"The value of num5 = {num5}");

            Console.WriteLine($"num1 + num2 = {num1 + num2}");
            Console.WriteLine($"num3 + num4 = {num3 + num4}");
            Console.WriteLine($"num5 + num6 = {num5 + num6}");

            // num2 = num2 +num1 shorthand
            Console.WriteLine($"{num2+=num1}");
            //num3 = num3 + num4
            Console.WriteLine($"{num3+=num4}");
            // multipling a string causes compile error
            Console.WriteLine($"{a*3}");



            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
